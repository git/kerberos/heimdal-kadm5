Source: libheimdal-kadm5-perl
Section: perl
Priority: optional
Build-Depends:
 debhelper (>= 11),
 heimdal-dev,
 perl,
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Russ Allbery <rra@debian.org>
Standards-Version: 4.1.3
Homepage: https://metacpan.org/release/Heimdal-Kadm5
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libheimdal-kadm5-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libheimdal-kadm5-perl

Package: libheimdal-kadm5-perl
Architecture: any
Depends:
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends},
Description: Perl module to administer a Heimdal Kerberos KDC
 Heimdal::Kadm5 is a Perl module that wraps the Heimdal libkadm5clnt
 library and allows administration of a Heimdal KDC inside Perl programs.
 It mimics the commands that would normally be sent to the server with the
 kadmin command.  Principal creation, deletion, modification, and
 searching and extraction of keytabs are supported.
 .
 This module is equivalent to Authen::Krb5::Admin except for a Heimdal KDC
 instead of an MIT Kerberos KDC.
