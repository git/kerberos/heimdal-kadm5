Description: Call the public kadm5 API functions instead of the internal ones
Author: Russ Allbery <rra@debian.org>

The kadm5_c_* functions are an internal API.  The public functions
are the ones without the _c, which dispatch to the internal functions
depending on whether one links with the client library or the server
library.  By calling the _c functions, we don't get the benefit of
prototype checking and fail if the internal function signature ever
changes.

The latter has now happened with Heimdal 1.6 currently used in Debian,
and as a result the create_principal and chpass_principal functions
here have stopped working because they provide the wrong number of
arguments and the Heimdal library sees random stack garbage as the
additional function arguments.

Replace all C calls to use the versions without _c, since this module
doesn't attempt to support the _s and _c interfaces simultaneously
anyway and therefore no benefit accrues from calling the _c functions.
This also fixes the backward-compatibility problem, since the public
API did not change, only the internal function API.

diff --git a/Kadm5.xs b/Kadm5.xs
index f1312f8..8ee5d73 100644
--- a/Kadm5.xs
+++ b/Kadm5.xs
@@ -218,10 +218,10 @@ DESTROY(handle)
      {
        if (handle->modcount > 0)
 	 {
-	   kadm5_c_flush(handle->ptr);
+	   kadm5_flush(handle->ptr);
 	 }
        if (handle->ptr)
-          kadm5_c_destroy(handle->ptr);
+          kadm5_destroy(handle->ptr);
        if (handle->context)
           krb5_free_context(handle->context);
        safefree(handle);
@@ -237,7 +237,7 @@ kadm5_c_init_with_password (handle, client_name, password, service_name, struct_
      unsigned long api_version
      CODE:
      {
-       kadm5_ret_t ret = kadm5_c_init_with_password_ctx(handle->context,
+       kadm5_ret_t ret = kadm5_init_with_password_ctx(handle->context,
 							client_name,
 							password,
 							KADM5_ADMIN_SERVICE, 
@@ -246,7 +246,7 @@ kadm5_c_init_with_password (handle, client_name, password, service_name, struct_
 							api_version,
 							&handle->ptr);
        if(ret)
-	    croak("[Heimdal::Kadm5] kadm5_c_init_with_password_ctx failed: %s\n",
+	    croak("[Heimdal::Kadm5] kadm5_init_with_password_ctx failed: %s\n",
 		  krb5_get_err_text(handle->context, ret));
 
        if (password != NULL && *password != '\0')
@@ -263,7 +263,7 @@ kadm5_c_init_with_skey (handle, client_name, keytab, service_name, struct_versio
      unsigned long api_version
      CODE:
      {
-       kadm5_ret_t ret = kadm5_c_init_with_skey_ctx(handle->context,
+       kadm5_ret_t ret = kadm5_init_with_skey_ctx(handle->context,
 						    client_name,
 						    keytab,
 						    KADM5_ADMIN_SERVICE, 
@@ -272,7 +272,7 @@ kadm5_c_init_with_skey (handle, client_name, keytab, service_name, struct_versio
 						    api_version,
 						    &handle->ptr);
        if(ret)
-	    croak("[Heimdal::Kadm5] kadm5_c_init_with_skey_ctx failed: %s\n",
+	    croak("[Heimdal::Kadm5] kadm5_init_with_skey_ctx failed: %s\n",
 		  krb5_get_err_text(handle->context, ret));
      }
 
@@ -281,9 +281,9 @@ kadm5_c_flush(handle)
      shandle_t *handle
      CODE:
      {
-       kadm5_ret_t ret = kadm5_c_flush(handle->ptr);
+       kadm5_ret_t ret = kadm5_flush(handle->ptr);
        if (ret)
-	 croak("[Heimdal::Kadm5] kadm5_c_flush failed: %s\n",krb5_get_err_text(handle->context, ret));
+	 croak("[Heimdal::Kadm5] kadm5_flush failed: %s\n",krb5_get_err_text(handle->context, ret));
        handle->modcount = 0;
      }
 
@@ -298,11 +298,11 @@ kadm5_c_modify_principal(handle,spp,mask)
 
        if (mask == 0)
 	 mask = spp->mask;
-       ret = kadm5_c_modify_principal(handle->ptr, &spp->principal, mask);
+       ret = kadm5_modify_principal(handle->ptr, &spp->principal, mask);
        if (ret)
 	 {
 	   if (ret)
-	     croak("[Heimdal::Kadm5] kadm5_c_modify_principal failed: %s\n",
+	     croak("[Heimdal::Kadm5] kadm5_modify_principal failed: %s\n",
 		   krb5_get_err_text(handle->context, ret));
 	 }
        handle->modcount++;
@@ -329,7 +329,7 @@ kadm5_c_randkey_principal(handle,name)
        if(ret)
 	 {
 	   krb5_free_principal(handle->context, principal);
-	   croak("[Heimdal::Kadm5] kadm5_c_randkey_principal failed: %s\n",
+	   croak("[Heimdal::Kadm5] kadm5_randkey_principal failed: %s\n",
 		 krb5_get_err_text(handle->context, ret));
 	 }
        for(i = 0; i < n_keys; i++)
@@ -358,9 +358,9 @@ kadm5_c_chpass_principal(handle,name,password)
 	 croak("[Heimdal::Kadm5] krb5_parse_name failed on \"%s\": %s\n",
 	       name,krb5_get_err_text(handle->context, ret2));
        
-       ret = kadm5_c_chpass_principal(handle->ptr,principal,password);
+       ret = kadm5_chpass_principal(handle->ptr,principal,password);
        if (ret)
-	 croak("[Heimdal::Kadm5] kadm5_c_chpass_principal failed on \"%s\": %s\n",
+	 croak("[Heimdal::Kadm5] kadm5_chpass_principal failed on \"%s\": %s\n",
 	       name,krb5_get_err_text(handle->context, ret));
        handle->modcount++;
      }
@@ -378,7 +378,7 @@ kadm5_c_create_principal(handle,spp,password,mask)
        if (mask == 0)
 	 mask = spp->mask;
        
-       ret = kadm5_c_create_principal(handle->ptr,&spp->principal,mask,password);
+       ret = kadm5_create_principal(handle->ptr,&spp->principal,mask,password);
        if (ret)
 	 {
 	   char *p;
@@ -391,7 +391,7 @@ kadm5_c_create_principal(handle,spp,password,mask)
 	       croak("[Heimdal::Kadm5] krb5_unparse_name failed: %s\n",
 		     krb5_get_err_text(spp->handle->context, ret2));
 	     }
-	   croak("[Heimdal::Kadm5] krb5_c_create_principal failed on \"%s\": %s\n",
+	   croak("[Heimdal::Kadm5] krb5_create_principal failed on \"%s\": %s\n",
 		 p,krb5_get_err_text(handle->context, ret));
 	 }
        handle->modcount++;
@@ -423,7 +423,7 @@ kadm5_c_rename_principal(handle, src, trg)
 		 trg,krb5_get_err_text(handle->context, ret));
 	 }
        
-       err = kadm5_c_rename_principal(handle->ptr, source, target);
+       err = kadm5_rename_principal(handle->ptr, source, target);
        if (err)
 	 {
 	   krb5_free_principal(handle->context, source);
@@ -451,11 +451,11 @@ kadm5_c_delete_principal(handle,name)
 	 croak("[Heimdal::Kadm5] krb5_parse_name failed on \"%s\": %s\n",
 	       name,krb5_get_err_text(handle->context, ret));
        
-       err = kadm5_c_delete_principal(handle->ptr,principal);
+       err = kadm5_delete_principal(handle->ptr,principal);
        if (err)
 	 {
 	   krb5_free_principal(handle->context, principal);
-	   croak("[Heimdal::Kadm5] kadm5_c_delete_principal failed for \"%s\": %s\n",
+	   croak("[Heimdal::Kadm5] kadm5_delete_principal failed for \"%s\": %s\n",
 		 name,krb5_get_err_text(handle->context, err));
 	 }
        handle->modcount++;
@@ -480,7 +480,7 @@ kadm5_c_get_principal(handle, name, mask)
 	       name,krb5_get_err_text(handle->context, ret));
        
        spp = create_sprincipal(handle);
-       ret = kadm5_c_get_principal(handle->ptr,
+       ret = kadm5_get_principal(handle->ptr,
 				   principal,
 				   &spp->principal,
 				   mask);
@@ -492,7 +492,7 @@ kadm5_c_get_principal(handle, name, mask)
            } else {
 	      krb5_free_principal(handle->context, principal);
               destroy_sprincipal(spp); 
-	      croak("[Heimdal::Kadm5] kadm5_c_get_principal failed for \"%s\": %s\n",
+	      croak("[Heimdal::Kadm5] kadm5_get_principal failed for \"%s\": %s\n",
 		    name,krb5_get_err_text(handle->context, ret));
            }
 	 }
@@ -512,10 +512,10 @@ kadm5_c_get_principals(handle,exp)
        int num_princs,i;
        kadm5_ret_t ret;
 
-       ret = kadm5_c_get_principals(handle->ptr,exp,&princs,&num_princs);
+       ret = kadm5_get_principals(handle->ptr,exp,&princs,&num_princs);
        if (ret)
 	 {
-	   croak("[Heimdal::Kadm5] kadm5_c_get_principals failed for \"%s\": %s\n",
+	   croak("[Heimdal::Kadm5] kadm5_get_principals failed for \"%s\": %s\n",
 		 exp,krb5_get_err_text(handle->context, ret));
 	 }
        EXTEND(SP,num_princs);
@@ -532,10 +532,10 @@ kadm5_c_get_privs(handle)
      CODE:
      {
        int privs;
-       kadm5_ret_t ret = kadm5_c_get_privs(handle->ptr,&privs);
+       kadm5_ret_t ret = kadm5_get_privs(handle->ptr,&privs);
        if (ret)
 	 {
-	   croak("[Heimdal::Kadm5] kadm5_c_get_privs failed: %s\n",
+	   croak("[Heimdal::Kadm5] kadm5_get_privs failed: %s\n",
 		 krb5_get_err_text(handle->context, ret));
 	 }
        RETVAL = privs;
